
## 1.0.1 : 21/04/2020 21:59 commit 23377204600fce1be50809277e948f1acfd6b459
* [Fix] research 'Explosive Rounds' having no effect.
* [Fix] a bug causing wave to be one-shoted in certain late-game conditions
* [Fix] a webgl bug causing visual glitch where the ennemy attack could be in front of the spaceship
* [Fix] sound 'level' related issue taking into account the requested sound level 2 times
* [Fix] two typos
* [Changes] Added more wave in the late game to force player's death
* [Changes] Enemies attack from further away to best match the shield size
* [Changes] Enemies attack animation now include small movements (was planed but disabled in 1.0.0 release)

## 1.0.0 : 21/04/2020 02:17 commit adb7f80cc5682f416c0701e50d78b23420730aba
* Initial Release
