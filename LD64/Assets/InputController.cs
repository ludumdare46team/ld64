﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : MonoBehaviour
{

    public Camera cameraElement;
    
    private Worker selectedWorker;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.game.paused)
        {
            if (Input.GetMouseButtonUp(0))
            {
                Ray ray = cameraElement.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D[] hits = new RaycastHit2D[10];
                ContactFilter2D filter = new ContactFilter2D();
                filter.NoFilter();
                Physics2D.Raycast(ray.origin, ray.direction, filter, hits);

                bool foundWorker = false;
                bool foundSomething = false;
                RoomType foundRoomType = null;
                foreach (RaycastHit2D hit in hits)
                {
                    if (hit)
                    {
                        Transform objectHit = hit.transform;

                        if (objectHit && objectHit.gameObject)
                        {
                            Worker hitEntity = objectHit.gameObject.GetComponent<Worker>();
                            if (hitEntity != null)
                            {
                                if (selectedWorker != null)
                                {
                                    selectedWorker.setSelected(false);
                                }
                                selectedWorker = hitEntity;
                                selectedWorker.setSelected(true);
                                foundWorker = true;
                                foundSomething = true;
                                break;
                            }

                            RoomType roomType = objectHit.gameObject.GetComponent<RoomType>();
                            if (roomType != null)
                            {
                                foundRoomType = roomType;
                                foundSomething = true;
                            }
                        }
                    }
                }
                if (foundSomething)
                {
                    GameSetup.setup.playEffect("Click");
                }

                if (!foundWorker)
                {
                    if (selectedWorker != null)
                    {
                        selectedWorker.setSelected(false);
                    }
                    selectedWorker = null;
                    if (foundRoomType != null)
                    {
                        GameController.game.selectRoom(foundRoomType);
                    }
                }
            }
            if (Input.GetMouseButtonUp(1))
            {
                Ray ray = cameraElement.ScreenPointToRay(Input.mousePosition);
                RaycastHit2D[] hits = new RaycastHit2D[10];
                ContactFilter2D filter = new ContactFilter2D();
                filter.NoFilter();
                Physics2D.Raycast(ray.origin, ray.direction, filter, hits);

                foreach (RaycastHit2D hit in hits)
                {
                    if (hit)
                    {
                        Transform objectHit = hit.transform;

                        if (objectHit && objectHit.gameObject)
                        {
                            Room hitRoom = objectHit.gameObject.GetComponent<Room>();
                            if (hitRoom != null && selectedWorker)
                            {
                                WorkStation workStation = hitRoom.GetNextAvailableWorkstation();
                                if (workStation != null)
                                {
                                    GameSetup.setup.playEffect("Click");
                                    selectedWorker.GoTo(workStation.relatedNode);
                                    hitRoom.ReserveEmplacement(workStation, selectedWorker);
                                    selectedWorker.workStation = workStation;
                                    selectedWorker.SetJob(hitRoom);
                                    workStation.currentlyAssignedTo = selectedWorker;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
