﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyWhenAnimDone : MonoBehaviour
{
    Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        AnimatorStateInfo asi = anim.GetCurrentAnimatorStateInfo(0);
        if (asi.IsName("done"))
        {
            Destroy(gameObject);
        }
    }
}
