﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Probe : MonoBehaviour
{
    public Vector3 destination;
    public float reductionQuantity;
    public float speed;
    public float MinScaleBeforeDisapear;

    private Vector3 direction;

    // Start is called before the first frame update
    void Start()
    {
        direction = (destination - transform.position).normalized;
        float angle = Vector3.Angle(new Vector3(1, 0, 0), direction);
        transform.rotation = Quaternion.Euler(0,0,angle);
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.game.paused)
        {
            transform.Translate(new Vector3(speed * Time.deltaTime, 0, 0));
            if (transform.localScale.x > MinScaleBeforeDisapear)
            {
                transform.localScale = new Vector3(transform.localScale.x - reductionQuantity * Time.deltaTime, transform.localScale.y - reductionQuantity * Time.deltaTime, transform.localScale.z);
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
