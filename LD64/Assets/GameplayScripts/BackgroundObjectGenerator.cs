﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BackgroundObjectGenerator : MonoBehaviour
{
    public GameObject bordablePrefab;
    public Vector3 spawnPosition;
    public Vector3 spawnPositionRandomness;

    [Serializable]
    public class RandomEventData
    {
        public Sprite texture;
        public float probability = 0.1f;
        public float sizeMultiplier = 1;
        public int Quantity = 1;

        public float selfVelocityX = 1;
        public float distMultiplier = 1;
    }
    public List<RandomEventData> randomEventList;

    class internalData<L>
    {
        public float proba;
        public L data;
    }
    List<internalData<RandomEventData>> dataList;
    float agreg = 0;
    float next = 0;

    public float minimalDelay = 5;
    public float maximalDelay = 30;

    // Start is called before the first frame update
    void Start()
    {
        float proba = 0;
        dataList = new List<internalData<RandomEventData>>();
        foreach (var randomEvent in randomEventList)
        {
            proba += randomEvent.probability;
            dataList.Add(new internalData<RandomEventData>
            {
                proba = proba,
                data = randomEvent
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameController.game.paused) {
            if (bordablePrefab)
            {
                agreg += Time.deltaTime;
                if (agreg > next)
                {
                    agreg -= next;
                    next = Random.Range(minimalDelay, maximalDelay);
                    checkEvent();
                }
            }
        }
    }

    void checkEvent()
    {
        float random = Random.Range(0f, 1f);
        foreach (var element in dataList)
        {
            if (element.proba > random )
            {
                TriggerEvent(element.data);
                return;
            }
        }
    }

    void TriggerEvent(RandomEventData data)
    {
        var obj = Instantiate(bordablePrefab);
        float dist = Random.Range(8f, 20f) * data.distMultiplier;

        var SpriteRenderer = obj.GetComponent<SpriteRenderer>();
        SpriteRenderer.sprite = data.texture;
        SpriteRenderer.sortingOrder = (int) -(dist * 10);
        // SpriteRenderer.

        var backgroundMoving = obj.GetComponent<BackgroundMoving>();
        backgroundMoving.speed = 25 / dist;
        backgroundMoving.vx = Random.Range(-1f, 1f) * data.selfVelocityX;
        obj.transform.position = spawnPosition + new Vector3(Random.Range(-1f,1f) * spawnPositionRandomness.x, data.sizeMultiplier + Random.Range(-1f, 1f) * spawnPositionRandomness.y, 0);
        obj.transform.localScale = Vector3.one * data.sizeMultiplier * 25 / dist;
        obj.transform.Rotate(0 , 0, Random.Range(0f,360f));
    }
}
