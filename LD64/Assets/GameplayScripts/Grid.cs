﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grid : MonoBehaviour
{
    public int gridSizeX;
    public int gridSizeY;

    private List<Node> nodes;
    private Room[] rooms;
    private Door[] doors;
    private GridBlocker[] blockers;

    public Room idleRoom;
    public WorkStation workStationPrefab;

    // Start is called before the first frame update
    void Start()
    {
        rooms = GetComponentsInChildren<Room>();
        doors = GetComponentsInChildren<Door>();
        blockers = GetComponentsInChildren<GridBlocker>();

        nodes = new List<Node>();
        for(int y = 0; y < gridSizeY; y++)
        {
            for(int x = 0; x < gridSizeX; x++)
            {
                nodes.Add(new Node(x, y));
            }
        }

        foreach (Room r in rooms)
        {
            r.setGrid(this);
            r.GetComponent<SpriteRenderer>().enabled = false;

            if(r.idleRoom)
            {
                this.idleRoom = r;
            }

            int sizeRoomX = Mathf.RoundToInt(r.transform.lossyScale.x);
            int sizeRoomY = Mathf.RoundToInt(r.transform.lossyScale.y);
            int roomBeginX = Mathf.RoundToInt(r.transform.position.x - r.transform.lossyScale.x / 2);
            int roomBeginY = Mathf.RoundToInt(r.transform.position.y - r.transform.lossyScale.y / 2);
            int roomEndX = Mathf.RoundToInt(r.transform.position.x + r.transform.lossyScale.x / 2);
            int roomEndY = Mathf.RoundToInt(r.transform.position.y + r.transform.lossyScale.y / 2);

            for (int y = roomBeginY; y < roomEndY; y++)
            {
                for (int x = roomBeginX; x < roomEndX; x++)
                {
                    if(r.idleRoom)
                    {
                        WorkStation ws = Instantiate<WorkStation>(workStationPrefab, new Vector3(x + 0.5f, y + 0.5f, 0.0f), new Quaternion(), r.transform);
                        ws.relatedNode = nodes[y * gridSizeX + x];
                        ws.workPosition = 3;
                    }
                    nodes[y * gridSizeX + x].relatedRoom = r;
                    if (y > roomBeginY)
                    {
                        nodes[y * gridSizeX + x].connectTo(nodes[(y - 1) * gridSizeX + x]);
                        if (x > roomBeginX)
                        {
                            nodes[y * gridSizeX + x].connectTo(nodes[(y - 1) * gridSizeX + x - 1]);
                        }
                        if (x < roomEndX - 1)
                        {
                            nodes[y * gridSizeX + x].connectTo(nodes[(y - 1) * gridSizeX + x + 1]);
                        }
                    }
                    if (y < roomEndY - 1)
                    {
                        nodes[y * gridSizeX + x].connectTo(nodes[(y + 1) * gridSizeX + x]);
                        if (x > roomBeginX)
                        {
                            nodes[y * gridSizeX + x].connectTo(nodes[(y + 1) * gridSizeX + x - 1]);
                        }
                        if (x < roomEndX - 1)
                        {
                            nodes[y * gridSizeX + x].connectTo(nodes[(y + 1) * gridSizeX + x + 1]);
                        }
                    }

                    if (x > roomBeginX)
                    {
                        nodes[y * gridSizeX + x].connectTo(nodes[y * gridSizeX + x - 1]);
                    }
                    if (x < roomEndX - 1)
                    {
                        nodes[y * gridSizeX + x].connectTo(nodes[y * gridSizeX + x + 1]);
                    }
                }
            }

            if(r.idleRoom)
            {
                r.updateWorkStations();
            }
        }

        foreach (Door d in doors)
        {
            d.GetComponent<SpriteRenderer>().enabled = false;
            bool vertical = Mathf.Abs(d.transform.position.x - Mathf.Round(d.transform.position.x)) > Mathf.Abs(d.transform.position.y - Mathf.Round(d.transform.position.y));

            if(vertical)
            {
                int x = Mathf.FloorToInt(d.transform.position.x);
                int y = Mathf.RoundToInt(d.transform.position.y);
                nodes[(y - 1) * gridSizeX + x].connectTo(nodes[y * gridSizeX + x]);
            }
            else
            {
                int x = Mathf.RoundToInt(d.transform.position.x);
                int y = Mathf.FloorToInt(d.transform.position.y);
                nodes[y * gridSizeX + (x - 1)].connectTo(nodes[y * gridSizeX + x]);
            }
        }

        foreach (GridBlocker gb in blockers)
        {
            gb.GetComponent<SpriteRenderer>().enabled = false;
            int x = Mathf.FloorToInt(gb.transform.position.x);
            int y = Mathf.FloorToInt(gb.transform.position.y);
            nodes[y * gridSizeX + x].disconnectAll();
        }
    }

    public Vector3 localCoordsToPosition(int _x, int _y)
    {
        return new Vector3(_x + 0.5f, _y + 0.5f, 0);
    }

    public Vector3 getNodePosition(Node _node)
    {
        return localCoordsToPosition(_node.posX, _node.posY);
    }

    public Node getNodeAtPos(Vector3 pos)
    {
        return nodes[Mathf.FloorToInt(pos.y) * gridSizeX + Mathf.FloorToInt(pos.x)];
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

public class Node
{
    public int posX;
    public int posY;
    public Room relatedRoom;
    public List<Node> neighbours;

    public Node(int _x, int _y)
    {
        posX = _x;
        posY = _y;
        neighbours = new List<Node>();
    }

    public void connectTo(Node _other)
    {
        if(!neighbours.Contains(_other))
        {
            neighbours.Add(_other);
        }
        if(!_other.neighbours.Contains(this))
        {
            _other.neighbours.Add(this);
        }
    }

    public void disconnectAll()
    {
        foreach(Node n in neighbours)
        {
            n.neighbours.Remove(this);
        }
        if(relatedRoom != null)
        {
            relatedRoom.RemoveWorkStationAtNode(this);
        }
        neighbours.Clear();
    }
}

