﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BoardEventGenerator : MonoBehaviour
{
    public GameObject bordablePrefab;
    public Vector3 spawnPosition;

    [Serializable]
    public class BoardEventData
    {
        // This is just for display reason
        public Sprite texture;
        public float sizeMultiplier = 1f;

        // This is the actual data
        public float frequencyMultiplier = 1f;
        public float mineralFrom = 1f;
        public float mineralTo = 1f;
        public float deathRate = 0.03f;
    }
    public List<BoardEventData> randomEventList;

    class internalData<L>
    {
        public float proba;
        public L data;
    }
    List<internalData<BoardEventData>> dataList;

    float agreg = 0;
    float next = 0;
    // we will use this to have a 100% rate
    float probaCount = 0;


    // Start is called before the first frame update
    void Start()
    {
        float proba = 0;
        dataList = new List<internalData<BoardEventData>>();
        foreach (var randomEvent in randomEventList)
        {
            proba += randomEvent.frequencyMultiplier;
            dataList.Add(new internalData<BoardEventData>
            {
                proba = proba,
                data = randomEvent
            });
        }
        probaCount = proba;
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameController.game.paused) {
            if (bordablePrefab)
            {
                agreg += Time.deltaTime;
                if (agreg > next)
                {
                    agreg -= next;
                    next = 30;
                    checkEvent();
                }
            }
        }
    }

    void checkEvent()
    {
        float random = Random.Range(0f, probaCount);
        foreach (var element in dataList)
        {
            if (element.proba >= random )
            {
                // this is the event yay !
                TriggerEvent(element.data);
                return;
            }
        }
    }

    void TriggerEvent(BoardEventData data)
    {
        var obj = Instantiate(bordablePrefab);

        var SpriteRenderer = obj.GetComponent<SpriteRenderer>();
        SpriteRenderer.sprite = data.texture;

        var backgroundMoving = obj.GetComponent<BackgroundMoving>();
        backgroundMoving.speed = 1f;
        obj.transform.Translate(spawnPosition);
        obj.transform.localScale = Vector3.one * data.sizeMultiplier * Random.Range(3f, 6f);

        var ressouces = obj.GetComponent<Ressources>();
        ressouces.crystals = (int) Random.Range(data.mineralFrom, data.mineralTo);
    }
}
