﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shield : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (GameController.game.getShieldPoints() > 100)
        {
            var shieldPourcentage = GameController.game.getShieldPoints() / GameController.game.getTotalShieldPoints();
            var baseAlpha = 0.25f;
            spriteRenderer.color = new Color(1, 1, 1, baseAlpha + shieldPourcentage * (1 - baseAlpha));
        } else {
            spriteRenderer.color = new Color(1, 1, 1, 0);
        }
    }
}
