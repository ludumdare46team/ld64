﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorkStation : MonoBehaviour
{
    public Grid grid;
    public Worker currentlyAssignedTo;
    public Node relatedNode;
    public int workPosition;

    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (grid != null && relatedNode == null)
        {
            relatedNode = grid.getNodeAtPos(new Vector2(transform.position.x, transform.position.y));
        }
    }
}
