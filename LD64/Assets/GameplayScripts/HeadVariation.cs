﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "HeadVariation")]
public class HeadVariation : ScriptableObject
{
    public Sprite FaceUp;
    public Sprite FaceRight;
    public Sprite FaceDown;
    public Sprite FaceLeft;
}
