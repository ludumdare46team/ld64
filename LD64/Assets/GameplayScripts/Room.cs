﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room : MonoBehaviour
{
    public bool idleRoom;

    private Grid grid;
    private List<Worker> workerList;
    private WorkStation[] workStations;

    private RoomType type;

    // Start is called before the first frame update
    void Start()
    {
        workerList = new List<Worker>();
        workStations = GetComponentsInChildren<WorkStation>();
        type = GetComponent<RoomType>();
        GetComponent<SpriteRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.game.paused)
        {
            if (type != null)
                type.updateRoomEffect(this);
        }
    }

    public void updateWorkStations()
    {
        workStations = GetComponentsInChildren<WorkStation>();
        foreach(WorkStation ws in workStations)
        {
            ws.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public void setGrid(Grid _grid)
    {
        if(workStations == null)
        {
            workStations = GetComponentsInChildren<WorkStation>();
        }
        grid = _grid;
        foreach (WorkStation ws in workStations)
        {
            ws.grid = grid;
            ws.GetComponent<SpriteRenderer>().enabled = false;
        }
    }

    public WorkStation GetNextAvailableWorkstation()
    {
        foreach (WorkStation ws in workStations)
        {
            if (ws.currentlyAssignedTo == null)
            {
                return ws;
            }
        }
        return null;
    }

    public WorkStation GetWorkStationOfWorker(Worker _worker)
    {
        foreach (WorkStation ws in workStations)
        {
            if (ws.currentlyAssignedTo == _worker)
            {
                return ws;
            }
        }
        return null;
    }

    public WorkStation GetWorkStationAtNode(Node _node)
    {
        foreach (WorkStation ws in workStations)
        {
            if (ws.relatedNode == _node)
            {
                return ws;
            }
        }
        return null;
    }
    

    public WorkStation RemoveWorkStationAtNode(Node _node)
    {
        foreach (WorkStation ws in workStations)
        {
            if (ws.relatedNode == _node)
            {
                Destroy(ws);
                updateWorkStations();
            }
        }
        return null;
    }

    public void ReserveEmplacement(WorkStation _station, Worker _worker)
    {
        if(_station.currentlyAssignedTo == null)
        {
            _station.currentlyAssignedTo = _worker;
        }
    }

    public void FreeEmplacement(WorkStation _station)
    {
        _station.currentlyAssignedTo = null;
    }

    public void CheckInWorker(Worker _worker)
    {
        if(!workerList.Contains(_worker))
        {
            workerList.Add(_worker);
        }
    }

    public void CheckOutWorker(Worker _worker)
    {
        if (workerList.Contains(_worker))
        {
            workerList.Remove(_worker);
        }

        foreach(WorkStation ws in workStations)
        {
            if(ws.currentlyAssignedTo == _worker)
            {
                FreeEmplacement(ws);
            }
        }
    }

    public int getActiveWorkerCount()
    {
        return workerList.Count;
    }

    public void destroyWorker()
    {
        if(workerList.Count > 0)
        {
            Worker w = workerList[0];
            CheckOutWorker(w);
            Destroy(w.gameObject);
        }
        
    }

    public int workStationCount()
    {
        return workStations.Length;
    }
}
