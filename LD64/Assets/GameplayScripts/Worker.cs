﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Animations;

public class Worker : GridEntity
{

    public SpriteRenderer tenueSprite;
    public SpriteRenderer headSprite;
    public SpriteRenderer colorSprite;
    public SpriteRenderer selectionCircle;

    public HeadVariation[] colorVariations;
    public HeadVariation[] headVariations;

    public WorkStation workStation;

    private Animator animator;

    private int colorIndex;
    private int headIndex;
    private int lastDirection;

    // Start is called before the first frame update
    void Start()
    {
        GameController.game.CrewCount++;

        colorIndex = Random.Range(0, colorVariations.Length);
        headIndex = Random.Range(0, headVariations.Length);

        headSprite.sprite = headVariations[headIndex].FaceDown;
        colorSprite.sprite = colorVariations[colorIndex].FaceDown;

        lastDirection = 0;

        animator = GetComponentInChildren<Animator>();
    }

    Room currentJob;

    public void SetJob(Room _room)
    {
        if(currentJob != null) {
            currentJob.CheckOutWorker(this);
        }
        currentState = ActionState.Idle;
        currentJob = _room;

    }

    protected override void UpdateState()
    {
        switch (currentState)
        {
            case ActionState.Idle:
                if (currentPath != null && currentPath.Count > 0)
                {
                    currentState = ActionState.Walking;
                }
                else
                {
                    if (currentJob == null)
                    {
                        WorkStation newWs = currentNode.relatedRoom.GetNextAvailableWorkstation();
                        if (newWs != null && newWs.relatedNode != null)
                        {
                            GoTo(newWs.relatedNode);
                            currentJob = currentNode.relatedRoom;
                            currentNode.relatedRoom.ReserveEmplacement(newWs, this);
                        }
                    }
                    else
                    {
                        currentJob.CheckInWorker(this);
                        currentState = ActionState.Working;
                    }
                }
                break;
            case ActionState.Walking:
                if (currentPath == null || currentPath.Count == 0)
                {
                    if (currentJob != null)
                    {
                        currentJob.CheckInWorker(this);
                        currentState = ActionState.Working;
                    }
                    else
                    {
                        currentState = ActionState.Idle;
                    }
                }
                break;
            case ActionState.Spawned:
                WorkStation idleWorkStation = grid.idleRoom.GetNextAvailableWorkstation();
                if (idleWorkStation != null && idleWorkStation.relatedNode != null)
                {
                    grid.idleRoom.ReserveEmplacement(idleWorkStation, this);
                    GoTo(idleWorkStation.relatedNode);
                    workStation = idleWorkStation;
                    SetJob(grid.idleRoom);
                    idleWorkStation.currentlyAssignedTo = this;
                }
                break;
        }
    }

    protected override void UpdateRotation()
    {
        base.UpdateRotation();
        headSprite.sprite = headVariations[headIndex].FaceDown;
        colorSprite.sprite = colorVariations[colorIndex].FaceDown;

        int direction = 3;
        if(currentState == ActionState.Walking)
        {
            Vector3 nextPos = grid.getNodePosition(currentPath[currentPath.Count - 1].node);
            Vector3 directionVector = nextPos - transform.position;

            if (Mathf.Abs(directionVector.x) > Mathf.Abs(directionVector.y))
            {
                if(directionVector.x > 0)
                {
                    direction = 2;
                }
                else
                {
                    direction = 4;
                }
            }
            else
            {
                if (directionVector.y > 0)
                {
                    direction = 1;
                }
                else
                {
                    direction = 3;
                }
            }
        }
        else if(currentState == ActionState.Working)
        {
            if (workStation == null)
            {
                workStation = currentJob.GetWorkStationOfWorker(this);
            }
            else
            {
                direction = workStation.workPosition;
            }
        }

        if (animator)
        {
            animator.SetBool("IsWalking", currentState == ActionState.Walking);
        }

        if (direction != lastDirection)
        {
            if (animator)
            {
                animator.SetInteger("Direction", direction);
            }
            switch (direction)
            {
                case 1:
                    headSprite.sprite = headVariations[headIndex].FaceUp;
                    colorSprite.sprite = colorVariations[colorIndex].FaceUp;
                    break;
                case 2:
                    headSprite.sprite = headVariations[headIndex].FaceRight;
                    colorSprite.sprite = colorVariations[colorIndex].FaceRight;
                    break;
                case 3:
                    headSprite.sprite = headVariations[headIndex].FaceDown;
                    colorSprite.sprite = colorVariations[colorIndex].FaceDown;
                    break;
                case 4:
                    headSprite.sprite = headVariations[headIndex].FaceLeft;
                    colorSprite.sprite = colorVariations[colorIndex].FaceLeft;
                    break;
            }
        }
    }

    public override bool GoTo(Node goal)
    {
        if(this.currentJob != null)
        {
            this.currentJob.CheckOutWorker(this);
            this.currentJob = null;
        }
        return base.GoTo(goal);
    }

    public void setSelected(bool _selected)
    {
        selectionCircle.enabled = _selected;
    }

    public void OnDestroy()
    {
        GameController.game.CrewCount--;
    }
}
