﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridEntity : MonoBehaviour
{

    public Grid grid;
    public Node currentNode;
    public float movementSpeed;
    protected List<PathfindingNode> currentPath;

    public ActionState currentState;

    public enum ActionState
    {
        Idle = 0,
        Walking = 1,
        Working = 2,
        Spawned = 3
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (!GameController.game.paused)
        {
            if (currentNode == null)
            {
                currentNode = grid.getNodeAtPos(new Vector2(transform.position.x, transform.position.y));
            }
            if (currentState == ActionState.Walking && currentPath != null && currentPath.Count > 0)
            {
                Vector3 nextPos = grid.getNodePosition(currentPath[currentPath.Count - 1].node);
                float dist = Vector3.SqrMagnitude(transform.position - nextPos);

                if (dist < movementSpeed * Time.deltaTime)
                {
                    transform.Translate((nextPos - transform.position).normalized * dist);
                    currentNode = currentPath[currentPath.Count - 1].node;
                    currentPath.RemoveAt(currentPath.Count - 1);
                }
                else
                {
                    transform.Translate((nextPos - transform.position).normalized * movementSpeed * Time.deltaTime);
                }
            }

            UpdateState();
            UpdateRotation();
        }
    }

    protected virtual void UpdateRotation()
    {

    }

    protected virtual void UpdateState()
    {
        switch(currentState)
        {
            case ActionState.Idle:
                if (currentPath != null && currentPath.Count > 0)
                {
                    currentState = ActionState.Walking;
                }
                break;
            case ActionState.Walking:
                if (currentPath == null || currentPath.Count == 0) {
                    currentState = ActionState.Idle;
                }
                break;
        }
    }

    public class PathfindingNode
    {
        public Node node;
        public PathfindingNode origin;
        public float cost;
        public float evaluatedTotalCost;
    }

    public virtual bool GoTo(Node goal)
    {
        List<PathfindingNode> openList = new List<PathfindingNode>();
        List<PathfindingNode> closedList = new List<PathfindingNode>();

        PathfindingNode startTile = new PathfindingNode();
        startTile.node = currentNode;
        startTile.cost = 0;
        startTile.evaluatedTotalCost = estimateRemainingDistance(currentNode, goal);

        openList.Add(startTile);

        while (openList.Count > 0)
        {
            PathfindingNode tile = null;
            float minCost = -1;
            foreach (PathfindingNode p in openList)
            {
                if (tile == null || p.evaluatedTotalCost < minCost)
                {
                    tile = p;
                    minCost = tile.evaluatedTotalCost;
                }
            }

            if (tile == null)
            {
                return false;
            }

            foreach (Node t in tile.node.neighbours)
            {
                bool available = true;
                foreach (PathfindingNode p in openList)
                {
                    if (p.node == t)
                    {
                        available = false;
                    }
                }
                foreach (PathfindingNode p in closedList)
                {
                    if (p.node == t)
                    {
                        available = false;
                    }
                }

                if (available)
                {
                    PathfindingNode newTile = new PathfindingNode();
                    newTile.node = t;
                    newTile.origin = tile;
                    newTile.cost = tile.cost + (newTile.node.posX != tile.node.posX && newTile.node.posY != tile.node.posY ? 1.6f : 1.0f);
                    newTile.evaluatedTotalCost = newTile.cost + estimateRemainingDistance(t, goal);

                    if (t == goal)
                    {
                        currentPath = new List<PathfindingNode>();
                        PathfindingNode endTile = newTile;
                        currentPath.Add(endTile);
                        while (endTile.origin != null)
                        {
                            endTile = endTile.origin;
                            currentPath.Add(endTile);
                        }
                        return true;
                    }
                    else
                    {
                        openList.Add(newTile);
                    }
                }
            }

            openList.Remove(tile);
            closedList.Add(tile);
        }

        return false;
    }

    float estimateRemainingDistance(Node start, Node end)
    {
        //return Mathf.Abs(start.posX - end.posX) + Mathf.Abs(start.posY - end.posY); // Manhattan because we don't care (to see if euclidian is needed)
        return Mathf.Sqrt(Mathf.Pow(start.posX - end.posX, 2) + Mathf.Pow(start.posY - end.posY, 2));
    }
}
