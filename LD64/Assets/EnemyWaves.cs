﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyWaves : MonoBehaviour
{
    public GameObject enemyPrefab;

    public Vector3 targetPosition;
    public float targetSize;
    private int spawnDist = 40;

    [Serializable]
    public class Wave
    {
        public float time;
        public int enemies;
    }

    public List<Wave> waves;

    private int index = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(!GameController.game.paused) {
            if (waves.Count > index)
            {
                if (waves[index].time < GameController.game.uiController.gameTime)
                {
                    SpawnEnemies(waves[index].enemies);
                    index++;
                }
            }
        }
    }

    void SpawnEnemies ( int count )
    {
        for (int i = 0; i < count; i++)
        {
            float ang = Random.Range(0, 2 * (float) Math.PI);
            float mydist = spawnDist * Random.Range(0.8f, 1.3f);
            float x = (float) Math.Cos(ang) * mydist + targetPosition.x;
            float y = (float) Math.Sin(ang) * mydist + targetPosition.y;

            var obj = Instantiate(enemyPrefab);
            obj.transform.position = new Vector3(x,y,0);

            var enemy = obj.GetComponent<EnemyController>();
            enemy.targetPosition = targetPosition;
            enemy.targetSize = targetSize;
        }
    }
}
