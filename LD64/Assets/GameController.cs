using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using Random = UnityEngine.Random;

public class GameController : MonoBehaviour
{

    public static GameController game;

    [Serializable]
    public class UpgradeCost {

        public float foodCost, mineralCost;
        public int humanCost;
    }

    [SerializeField]
    private List<UpgradeCost> probCosts;

    public Worker workerPrefab;
    public Grid starShipGrid;

    [SerializeField]
    private float totalShieldPoints, startFood, startMineral, enemyDPS, enemyHP, maxFood, maxMinerals;

    public GameUiController uiController;

    [SerializeField]
    private TMPro.TextMeshProUGUI regenText, playerDPSText;

    private float shieldPoints;

    private int _crewCount;
    public int CrewCount { 
        get => _crewCount;
        set {
            _crewCount = value;
            uiController.setCrewCount(value);
        }
    }

    private int _enemiesCount;
    public int EnemiesCount
    {
        get => _enemiesCount;
        set
        {
            _enemiesCount = value;
            uiController.setEnemiesCount(value);
        }
    }
    public List<EnemyController> Enemies { get; set; } = new List<EnemyController>();

    private float foodCount;
    private float mineralsCount;

    [HideInInspector]
    public float foodBoostCondition, foodBoost, killEnemyCrystalBonus, crystalBoostCondition, crystalBoost, cloneExtraChance, cloneShieldBonus, shieldProbeBonus, enemyCountExtraDamage, enemyCountExtraDamageBoost, doubleKillChance, probeFoodCostBonus;

    [HideInInspector]
    public int probsCount, probeHumanCostBonus;

    public List<RoomHoldValue> shieldRoomValues, weaponRoomValues;

    public GameObject probePrefab;
    public ParticleSystem explosion;
    void Awake()
    {
        game = this;
        setShieldPoints(totalShieldPoints);
        probsCount = 0;
        uiController.setProbsCount(probsCount);
        CrewCount = 0;
        EnemiesCount = 0;
        setFoodCount(startFood);
        setMineralsCount(startMineral);
        uiController.showSendProbUI(false);
    }

    [HideInInspector]
    public bool gameover, paused;

    // Start is called before the first frame update
    void Start()
    {
        gameover = false;
        GameSetup.setup.gameStart();
    }

    // Update is called once per frame
    void Update()
    {
        if (!gameover && !paused)
        {
            updateFight();

            if(getShieldPoints() <= 0)
            {
                gameover = true;
                uiController.gameOverPopup();
            }
        }

        if (!gameover && Input.GetKeyUp(KeyCode.Escape))
        {
            uiController.setPause(!paused);
        }

        setRegenCount(getShieldRegen());
        setPlayerDPSCount(getPlayerDPS());
    }

    void updateFight()
    {
        var enemyCount = Enemies.Count;
        float shield = shieldPoints + (getShieldRegen() - enemyCount * enemyDPS) * Time.deltaTime;
        setShieldPoints(shield);

        //player defending
        float dps = getPlayerDPS() * Time.deltaTime;

        if (enemyCount >= enemyCountExtraDamage)
        {
            dps += enemyCountExtraDamageBoost * Time.deltaTime;
        }

        while (Enemies.Count > 0 && dps > 0)
        {
            var enemy = Enemies[0];
            if (enemy.hitPoint > 0)
            {
                var hitpoint = enemy.hitPoint;

                enemy.hitPoint -= dps;
                
                dps -= hitpoint;
            }

            if (enemy.hitPoint <= 0)
            {
                killEnemy(enemy);
                
                if (doubleKillChance > Random.Range(0.0f, 100.0f))
                {
                    if(Enemies.Count > 0)
                    {
                        killEnemy(Enemies[0]);
                    }
                }
            }
        }
    }

    void killEnemy(EnemyController enemy)
    {
        EnemiesCount--;
        Enemies.RemoveAt(0);
        enemy.die();
        setMineralsCount(mineralsCount + killEnemyCrystalBonus);
    }

    public float getShieldPoints()
    {
        return shieldPoints;
    }

    public void setShieldPoints(float points)
    {
        if (points > totalShieldPoints)
            points = totalShieldPoints;

        if (points < 0)
            points = 0;
        shieldPoints = points;
        uiController.setShieldValue(shieldPoints);
    }


    public float getFoodCount()
    {
        return foodCount;
    }

    public void setFoodCount(float count)
    {
        foodCount = count;
        if (foodCount > maxFood)
            foodCount = maxFood;
        if (foodCount < 0)
            foodCount = 0;
        uiController.setFoodCount(count);
    }

    public float getMineralsCount()
    {
        return mineralsCount;
    }

    public void setMineralsCount(float count)
    {
        mineralsCount = count;
        if (mineralsCount > maxMinerals)
            mineralsCount = maxMinerals;
        if (mineralsCount < 0)
            mineralsCount = 0;
        
        uiController.setMineralsCount(count);
    }

    public float getTotalShieldPoints()
    {
        return totalShieldPoints;
    }

    public void addEnemy(EnemyController newEnemy)
    {
        EnemiesCount++;
        // kinda ugly
        newEnemy.hitPoint = enemyHP;
        Enemies.Add(newEnemy);
    }

    public void setRegenCount(float count)
    {
        
        regenText.text = "Shield Regen " + (int)count;
    }

    public float getShieldRegen()
    {
        float res = 0;
        foreach (RoomHoldValue rhv in shieldRoomValues)
        {
            res += rhv.value;
        }
        return res;
    }

    public float getPlayerDPS()
    {
        float res = 0;
        foreach (RoomHoldValue rhv in weaponRoomValues)
        {
            res += rhv.value;
        }
        return res;
    }

    public void setPlayerDPSCount(float count)
    {
        playerDPSText.text = "Player DPS " + (int)count;
    }

    private float cloneTimer;
    //room update methods
    public void updateCrewRoom(float rate)
    {
        
        if (rate != 0 && cloneTimer >= rate)
        {
            cloneTimer = 0;
            int quantity = 1;
            if(cloneExtraChance > Random.Range(0.0f, 100.0f))
            {
                quantity = 2;
            }
            setShieldPoints(shieldPoints + cloneShieldBonus);

            for(int i = 0; i < quantity; i++)
            {
                Worker worker = Instantiate(workerPrefab);
                GameSetup.setup.playEffect("CloneFinish");
                worker.grid = FindObjectsOfType<Grid>()[0]; // We have only one grid, right ?
                worker.transform.position = FindObjectsOfType<SpawnPoint>()[0].transform.position; // We should have only one too... i guess
                worker.currentState = GridEntity.ActionState.Spawned;
            }
        }
        else
        {
            cloneTimer += Time.deltaTime;
        }
    }
    public void updateFoodRoom(float rate)
    {
        if(foodBoostCondition < foodCount)
            rate += foodBoost;

        setFoodCount(foodCount + rate * Time.deltaTime);
    }
    public void updateMineralsRoom(float rate)
    {
        if (mineralsCount < crystalBoostCondition)
            rate += crystalBoost;
        setMineralsCount(mineralsCount + rate * Time.deltaTime);
    }


    private UpgradeCost currentProbCost;

    public UpgradeCost getProbCost()
    {
        int index = probsCount;
        if (index >= probCosts.Count)
            index = probCosts.Count - 1;

        if (currentProbCost == null)
            currentProbCost = new UpgradeCost();

        currentProbCost.foodCost = probCosts[index].foodCost + probeFoodCostBonus;
        if (currentProbCost.foodCost < 0)
            currentProbCost.foodCost = 0;
        currentProbCost.mineralCost = probCosts[index].mineralCost;
        if (currentProbCost.mineralCost < 0)
            currentProbCost.mineralCost = 0;
        currentProbCost.humanCost = probCosts[index].humanCost + probeHumanCostBonus;
        if (currentProbCost.humanCost < 0)
            currentProbCost.humanCost = 0;
        return currentProbCost;
    }

    public void updateProbRoom(float rate)
    {
        uiController.showSendProbUI(selectedRoom != null && selectedRoom.isProbRoom, rate);
    }

    public void sendProbe()
    {
        GameSetup.setup.playEffect("SeedJingle");
        Instantiate(probePrefab, new Vector3(8, 5, 0), new Quaternion());
        UpgradeCost costs = getProbCost();
        setFoodCount(foodCount - costs.foodCost);
        setMineralsCount(mineralsCount - costs.mineralCost);
        if(costs.humanCost > 0)
        {
            Room room = selectedRoom.GetComponent<Room>();
            room.destroyWorker();
        }
        setShieldPoints(shieldPoints + shieldProbeBonus);
        probsCount++;
        uiController.setProbsCount(probsCount);
    }

    private RoomType selectedRoom;
    public void selectRoom(RoomType room)
    {
        uiController.setRoom(room);
        selectedRoom = room;
    }

    public void buyUpgrade(RoomUpgrade upgrade, RoomType room)
    {
        setFoodCount(foodCount - upgrade.foodCost);
        setMineralsCount(mineralsCount - upgrade.mineralCost);
        room.addUpgrade(upgrade);
    }

    public void increaseTotaltShield(float value)
    {
        totalShieldPoints += value;
    }
}
