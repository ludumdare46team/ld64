﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameUiController : MonoBehaviour
{
    public Ressource shieldRessource, crewRessource, foodRessource, mineralRessource;
    public TMPro.TextMeshProUGUI timerText, shieldCountText, crewCountText, foodCountText, mineralCountText, enemiesCountText, probsCountText, roomNameText, roomInfoText, probSendDesc, endPopupEpilogueText;
    public Image roomIcon;
    public Slider shieldSlider;
    public Button probButton, buyUpgradeButton;
    public GameObject sendProbUI, endPopup, upgradeInfoPanel,upgradeTree5, upgradeTree4, upgradeOwned, pausedPopup;

    public TMPro.TextMeshProUGUI upgradeName, upgradeDesc;
    public List<Button> upgradeButton5, upgradeButton4;

    public Sprite upgradeUnlocked, upgradeUnlockedH, upgradeBuyable, upgradeBuyableH, upgradeLocked, upgradeLockedH;

    [Serializable]
    public class EpilogueOption
    {
        public int probCount;
        public string text;
    }

    public List<EpilogueOption> epilogues;
    [HideInInspector]
    public RoomType currentRoomType;
    [HideInInspector]
    public RoomUpgrade currentRoomUpgrade;

    public Animator uiAnimator;

    private SpriteState unlockedUpgradeSS, buyableUpgradeSS, lockedUpgradeSS;
    void Awake()
    {
        endPopup.SetActive(false);
        unlockedUpgradeSS = new SpriteState();
        unlockedUpgradeSS.highlightedSprite = upgradeUnlockedH;
        unlockedUpgradeSS.selectedSprite = upgradeUnlockedH;
        unlockedUpgradeSS.pressedSprite = upgradeUnlockedH;

        buyableUpgradeSS = new SpriteState();
        buyableUpgradeSS.highlightedSprite = upgradeBuyableH;
        buyableUpgradeSS.selectedSprite = upgradeBuyableH;
        buyableUpgradeSS.pressedSprite = upgradeBuyableH;

        lockedUpgradeSS = new SpriteState();
        lockedUpgradeSS.highlightedSprite = upgradeLockedH;
        lockedUpgradeSS.selectedSprite = upgradeLockedH;
        lockedUpgradeSS.pressedSprite = upgradeLockedH;
    }

    private string padSeconds ( int time )
    {
        if (time < 10)
            return "0" + time;
        return ""+time;
    }
    public float gameTime;
    void Update()
    {
        if (!GameController.game.gameover && !GameController.game.paused) {
            gameTime += Time.deltaTime;
            string formattedTime = (int)(gameTime / 60) + ":" + padSeconds((int)(gameTime % 60));
            timerText.text = formattedTime;

            if (currentRoomUpgrade != null && currentRoomType != null)
            {
                upgradeInfoPanel.SetActive(true);
                bool owned = currentRoomType.upgradeUnlocked.Contains(currentRoomUpgrade);
                buyUpgradeButton.gameObject.SetActive(!owned && currentRoomUpgrade.canBuy(currentRoomType));
                upgradeOwned.SetActive(owned);
            }
            else
            {
                upgradeInfoPanel.SetActive(false);
            }

            if (currentRoomType != null)
            {
                roomInfoText.text = currentRoomType.getDescription();
                roomInfoText.text += "\nCrew " + currentRoomType.GetComponent<Room>().getActiveWorkerCount() + "/" + currentRoomType.GetComponent<Room>().workStationCount();
            }

            if (currentRoomUpgrade != null)
                updateUpgradeInfo(currentRoomUpgrade);
        }
    }

    public void setShieldValue(float shieldPoint)
    {
        shieldSlider.value = shieldPoint / (float)GameController.game.getTotalShieldPoints();
        shieldCountText.text = (int)shieldPoint + " / " + GameController.game.getTotalShieldPoints();
    }

    public void setCrewCount(int count)
    {
        crewCountText.text = "Crew " + count;
    }

    public void setFoodCount(float count)
    {
        foodCountText.text = "Food " + (int)count;
    }

    public void setMineralsCount(float count)
    {
        mineralCountText.text = "Crystals " + (int)count;
    }

    public void setEnemiesCount(int count)
    {
        enemiesCountText.text = "Enemies " + count;
    }

    public void setProbsCount(int count)
    {
        probsCountText.text = "Probes " + count;
    }
    
    public void showSendProbUI(bool show, float rate = 0)
    {
        sendProbUI.SetActive(show);
        GameController.UpgradeCost costs = GameController.game.getProbCost();

        

        bool enoughFood = GameController.game.getFoodCount() >= costs.foodCost;
        bool enoughCrystal = GameController.game.getMineralsCount() >= costs.mineralCost;
        bool enoughHuman = (costs.humanCost == 0) || (rate > 0);
        if(enoughFood)
            probSendDesc.text = "<#00C6FF>Food Cost: " + costs.foodCost + "\n";
        else
            probSendDesc.text = "<#FF2600>Food Cost: " + costs.foodCost + "\n";
        if(enoughCrystal)
            probSendDesc.text += "<#00C6FF>Crystal Cost: " + costs.mineralCost + "\n";
        else
            probSendDesc.text += "<#FF2600>Crystal Cost: " + costs.mineralCost + "\n";

        if (costs.humanCost > 0)
        {   if(enoughHuman)
                probSendDesc.text += "<#00C6FF>Crew Cost: " + costs.humanCost + "\n";
            else
                probSendDesc.text += "<#FF2600>Crew Cost: " + costs.humanCost + "\n";
        }
            

   

        if (enoughFood && enoughCrystal && enoughHuman)
        {
            probSendDesc.text += "<#00C6FF>Ready to send!";
            probButton.interactable = true;
        }
        else
        {
            probButton.interactable = false;
        }
       
    }
  
    public void updateUpgradeButton(Button b, RoomUpgrade ru)
    {
        if (currentRoomType.upgradeUnlocked.Contains(ru))
        {
            b.image.sprite = upgradeUnlocked;
            b.spriteState = unlockedUpgradeSS;
        }else if (ru.requirementMet(currentRoomType))
        {
            b.image.sprite = upgradeBuyable;
            b.spriteState = buyableUpgradeSS;
        }
        else
        {
            b.image.sprite = upgradeLocked;
            b.spriteState = lockedUpgradeSS;
        }
       
    }

    public void setRoom(RoomType roomType)
    {
        currentRoomType = roomType;
        
        if (!currentRoomType.isProbRoom && currentRoomType.rank1_1 != null)
            upgradeButtonClick(0); 
        roomNameText.text = roomType.name;
        roomInfoText.text = roomType.getDescription();
        roomInfoText.text += "\nCrew " + roomType.GetComponent<Room>().getActiveWorkerCount() + "/" + roomType.GetComponent<Room>().workStationCount();
        roomIcon.sprite = roomType.sprite;

        // roomType.rank1_1
        //set upgrade panel
        if (!currentRoomType.isProbRoom)
        {
            if (currentRoomType.rank1_2 != null)
            {
                upgradeTree5.SetActive(true);
                upgradeTree4.SetActive(false);
                if (upgradeButton5.Count > 0 && currentRoomType.rank1_1 != null)
                {
                    updateUpgradeButton(upgradeButton5[0], currentRoomType.rank1_1);
                }

                if (upgradeButton5.Count > 1 && currentRoomType.rank1_2 != null)
                {
                    updateUpgradeButton(upgradeButton5[1], currentRoomType.rank1_2);
                }

                if (upgradeButton5.Count > 2 && currentRoomType.rank2_1 != null)
                {
                    updateUpgradeButton(upgradeButton5[2],currentRoomType.rank2_1);
                }

                if (upgradeButton5.Count > 3 && currentRoomType.rank2_2 != null)
                {
                    updateUpgradeButton(upgradeButton5[3], currentRoomType.rank2_2);
                }

                if (upgradeButton5.Count > 4 && currentRoomType.rank3 != null)
                {
                    updateUpgradeButton(upgradeButton5[4], currentRoomType.rank3);
                }
            }
            else
            {
                upgradeTree5.SetActive(false);
                upgradeTree4.SetActive(true);
                if (upgradeButton4.Count > 0 && currentRoomType.rank1_1 != null)
                {
                    updateUpgradeButton(upgradeButton4[0], currentRoomType.rank1_1);
                }

                if (upgradeButton4.Count > 1 && currentRoomType.rank2_1 != null)
                {
                    updateUpgradeButton(upgradeButton4[1], currentRoomType.rank2_1);
                }

                if (upgradeButton4.Count > 2 && currentRoomType.rank2_2 != null)
                {
                    updateUpgradeButton(upgradeButton4[2], currentRoomType.rank2_2);
                }

                if (upgradeButton4.Count > 3 && currentRoomType.rank3 != null)
                {
                    updateUpgradeButton(upgradeButton4[3], currentRoomType.rank3);
                }
            }
        }
        else
        {
            currentRoomUpgrade = null;
        }
    }

    public void gameOverPopup()
    {
        if (epilogues.Count < 1)
            return;

        int index = 0;
        while (index + 1 < epilogues.Count && epilogues[index + 1].probCount <= GameController.game.probsCount)
        {
            index++;
        }
        endPopupEpilogueText.text = epilogues[index].text;
        uiAnimator.SetBool("gameover", true);

        GameSetup.setup.gameOver(index);
    }

    public void upgradeButtonClick(int index)
    {
        switch (index)
        {
            case 0:
                {
                    currentRoomUpgrade = currentRoomType.rank1_1;
                    break;
                }
            case 1:
                {
                    currentRoomUpgrade = currentRoomType.rank1_2;
                    break;
                }
            case 2:
                {
                    currentRoomUpgrade = currentRoomType.rank2_1;
                    break;
                }
            case 3:
                {
                    currentRoomUpgrade = currentRoomType.rank2_2;
                    break;
                }
            default:
                {
                    currentRoomUpgrade = currentRoomType.rank3;
                    break;
                }
        }
        updateUpgradeInfo(currentRoomUpgrade);
    }

    public void updateUpgradeInfo(RoomUpgrade ru)
    {
        upgradeName.text = ru.name;
        //Red </ color > 
        upgradeDesc.text = "<#00C6FF>" + ru.desc;

        if (!currentRoomType.upgradeUnlocked.Contains(ru))
        {
            upgradeDesc.text += "\n\nCosts: ";
            if(ru.foodCost > GameController.game.getFoodCount())
                upgradeDesc.text += "<#FF2600>";
            else
                upgradeDesc.text += "<#00C6FF>";

            upgradeDesc.text += ru.foodCost + " food, ";

            if (ru.foodCost > GameController.game.getFoodCount())
                upgradeDesc.text += "<#FF2600>";
            else
                upgradeDesc.text += "<#00C6FF>";

            upgradeDesc.text += ru.mineralCost + " crystals";
        }

    }

    public void buyClick()
    {
        if(currentRoomUpgrade != null && currentRoomType != null)
            GameController.game.buyUpgrade(currentRoomUpgrade, currentRoomType);

        setRoom(currentRoomType);
    }

    public void setPause(bool on)
    {
        GameController.game.paused = on;
        pausedPopup.SetActive(GameController.game.paused);
    }
}
