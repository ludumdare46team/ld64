﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialController : MonoBehaviour
{

    [Serializable]
    public class tuto
    {
        public Sprite screenshot;
        public string text;
    }

    public List<tuto> tutos;
    public Image image;
    public TMPro.TextMeshProUGUI text;
    private int index;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.Return))
        {
            index++;
        }

        if (index < tutos.Count)
        {
            image.sprite = tutos[index].screenshot;
            text.text = tutos[index].text;
        }
        else
        {
            start();
        }
    }

    public void start()
    {
        Initiate.Fade("Game", Color.black, 1f);
    }
}
