﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
//Static object used to store settings needed throughout the whole game
// Singelton object, there can be only one in the whole application
public class GameSetup : MonoBehaviour
{
	private static GameSetup _setup;

    public static GameSetup setup { 
		get {
			if (_setup == null)
			{
				((GameObject)Instantiate(Resources.Load("Setup"))).GetComponent<GameSetup>();
			}
			return _setup;
		}
	}
	[HideInInspector]
	public float effectsVolume = 0.5f, musicVolume = 0.2f;
	public AudioSource currentMusic;

	public AudioClip MenuTheme;
	public AudioClip[] GameThemes;
	public AudioClip[] EndThemes;

	public LabelledSoundEffect[] SoundEffects;

	private int currentThemeIndex;
	public int state; // 0 = Menu, 1 = Game, 2 = Gameover

	[System.Serializable]
	public struct LabelledSoundEffect
	{
		public string effectID;
		public SoundEffect effect;
	}

	void Awake()
    {
		state = 0;
        if (!_setup)
        {
            _setup = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

	public void Update()
	{
		if(!currentMusic.isPlaying)
		{
			if(state == 1)
			{
				currentThemeIndex = (currentThemeIndex + 1) % GameThemes.Length;
				playMusic(GameThemes[currentThemeIndex], musicVolume, false);
			}
		}
	}

	private void playMusic(AudioClip clip, float volume, bool loop)
	{
		if (currentMusic != null && currentMusic.isPlaying)
		{
			currentMusic.DOFade(0f, 0.5f).OnComplete(() => {
				currentMusic.clip = clip;
				currentMusic.volume = musicVolume * volume;
				currentMusic.loop = loop;
				currentMusic.Play();
			});
		}
		else
		{
			currentMusic.clip = clip;
			currentMusic.volume = musicVolume * volume;
			currentMusic.loop = loop;
			currentMusic.Play();
		}
	}

	public void gameStart()
	{
		playMusic(GameThemes[0], 1, false);
		currentThemeIndex = 0;
		state = 1;
	}

	public void gameOver(int end)
	{
		playMusic(EndThemes[end], 1, false);
		state = 2;
	}

	public void playEffect(string _effectID)
	{
		foreach(LabelledSoundEffect effect in SoundEffects)
		{
			if(effect.effectID == _effectID)
			{
				effect.effect.PlayAt(new Vector3(0,0,0));
				break;
			}
		}
	}

}
