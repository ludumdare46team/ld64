﻿using UnityEngine;
using System;
using System.Collections.Generic;

public class Animation : MonoBehaviour
{
    [Serializable]
    public class Anim
    {
        public Sprite sprite;
        public float duration;
    }
    public GameObject SpritePrefab;
    public List<Anim> AnimationList;
    public bool isActivated = false;

    private int index = 0;
    private float animProgress;
    private GameObject internalSpriteObject;

    // Use this for initialization
    void Start()
    {
        animProgress = 0;
        internalSpriteObject = Instantiate(SpritePrefab);
        //internalSpriteObject.transform.parent = gameObject.transform;
        changeSprite();
    }

    void ResetPosition()
    {
        internalSpriteObject.transform.position = gameObject.transform.position + gameObject.transform.up * 2f;
        internalSpriteObject.transform.rotation = gameObject.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        if (!isActivated)
        {
            return;
        }
        animProgress += Time.deltaTime;
        if (index < 4)
        {
            ResetPosition();
        }
        if (AnimationList[index].duration < animProgress )
        {
            animProgress -= AnimationList[index].duration;
            index++;
            if ( index >= AnimationList.Count )
            {
                index = 0;
            }
            changeSprite();
        }
    }

    void changeSprite ()
    {
        var SpriteRenderer = internalSpriteObject.GetComponent<SpriteRenderer>();
        SpriteRenderer.sprite = AnimationList[index].sprite;
    }

    private void OnDestroy()
    {
        Destroy(internalSpriteObject);
    }
}
