﻿using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class EnemyController : MonoBehaviour
{
    internal Vector3 targetPosition;
    internal float targetSize;
    public float speed = 8;
    internal float hitPoint = 50;
    private bool isAttacking;
    private float rotationAngle;
  

    private float attackAnimationProgress = 0;
    private float attackAnimationSpeed;
    internal Vector3 attackPosition;

    public Animator anim;
    public float attackRate = 1.0f;

    private float timer;
    // Start is called before the first frame update
    void Start()
    {
        isAttacking = false;
        var delta = targetPosition - transform.position;
        var angle = -90 + (float)(Mathf.Rad2Deg * Math.Atan2(delta.y, delta.x));
        rotationAngle = angle - 90;
        var rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = rotation;
        transform.localScale = Vector3.one * Random.Range(0.85f, 1.2f);
        attackAnimationSpeed = Random.Range(0.8f, 1.3f);
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        if (!isAttacking)
        {
            var delta = targetPosition - transform.position;
            var dist = delta.magnitude;
            delta.Normalize();
            transform.position = transform.position + delta * speed * Time.deltaTime;
            if (dist < targetSize)
            {
                isAttacking = true;
                GameController.game.addEnemy(this);
                attackPosition = transform.position;
            }
        }
        else
        {
            attackAnimationProgress += Time.deltaTime * Mathf.PI * attackAnimationSpeed;
            transform.position = attackPosition + new Vector3(Mathf.Cos(attackAnimationProgress * 2), Mathf.Sin(attackAnimationProgress), 0) * 0.33f;

            if(timer > attackRate)
            {
                timer = 0;
                anim.SetTrigger("fire");
            }
        }
    }

    public void die()
    {
        // Do something !
        GameSetup.setup.playEffect("Enema");
        if(GameController.game.explosion != null)
        {
            GameController.game.explosion.transform.position = transform.position;
            GameController.game.explosion.Emit(1);
        }
        Destroy(gameObject);
    }
}
