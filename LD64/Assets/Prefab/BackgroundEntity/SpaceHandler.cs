﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceHandler : MonoBehaviour
{
    public GameObject spaceBackground;

    private GameObject upper, lower;

    // Start is called before the first frame update
    void Start()
    {
        upper = Instantiate(spaceBackground);
        upper.transform.position = new Vector3(9, 0, 0);
        lower = Instantiate(spaceBackground);
        lower.transform.position = new Vector3(9, -134.2f, 0);
    }

    // Update is called once per frame
    void Update()
    {
        var pos = upper.transform.position.y;
        if (pos < 0)
        {
            //var newLow = 
            lower.transform.position = new Vector3(9, pos, 0);
            upper.transform.position = new Vector3(9, pos + 134.2f, 0);
        }
    }
}
