using UnityEngine;

public class BackgroundMoving : MonoBehaviour
{
    private const int MAXDIST = 50;
    public float speed = 1;
    public float vx = 0;
    public float size = 1;

    private void remove ()
    {
        Destroy(gameObject);
        Destroy(this);
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + (new Vector3(vx,-speed,0) * Time.deltaTime);
        
        if ( transform.position.y > (MAXDIST + size) ||
            transform.position.y < -(MAXDIST + size) ||
            transform.position.x >  (MAXDIST + size) ||
            transform.position.x < -(MAXDIST + size))
        {
            remove();
        }
    }
}
