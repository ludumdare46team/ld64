﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SoundEffects/Simple")]
public class SimpleSoundEffect : SoundEffect
{

    public List<AudioClip> clips;

    [MinMaxRange(0, 2)]
    public RangedFloat volume;

    [MinMaxRange(0, 4)]
    public RangedFloat pitch;

    public override void PlayAt(Vector3 position)
    {

        float vol = Random.Range(volume.minValue, volume.maxValue);
        if (GameSetup.setup != null)
            vol *= GameSetup.setup.effectsVolume;

        AudioSource sound = MundoSound.Play(clips[Random.Range(0, clips.Count)], vol, position, false);
        sound.pitch = Random.Range(pitch.minValue, pitch.maxValue);
        
    }

    public override AudioSource StartAt(Vector3 position)
    {
        float vol = Random.Range(volume.minValue, volume.maxValue);
        if (GameSetup.setup != null)
            vol *= GameSetup.setup.effectsVolume;

        AudioSource sound = MundoSound.Play(clips[Random.Range(0, clips.Count)], vol, position, true);
        sound.pitch = Random.Range(pitch.minValue, pitch.maxValue);

            
        return sound;
    }
}
