﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(menuName = "IngameResource")]

public class Ressource : ScriptableObject
{
    public string name;
    public string description;
}