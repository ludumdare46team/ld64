﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public abstract class SoundEffect : ScriptableObject
{

    public abstract void PlayAt(Vector3 position);

    public abstract AudioSource StartAt(Vector3 position);
}



