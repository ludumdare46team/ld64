﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "RoomUpgrade")]
public class RoomUpgrade : ScriptableObject
{
    public string name;
    public string desc;
    public Sprite icon;
    public float foodCost, mineralCost;
    public string key;
    public float value, conditionValue;
    public List<RoomUpgrade> requirements;

    public bool canBuy(RoomType room)
    {
        return !room.upgradeUnlocked.Contains(this) && requirementMet(room) && GameController.game.getFoodCount() >= foodCost && GameController.game.getMineralsCount() >= mineralCost;
    }

    public bool requirementMet(RoomType room)
    {
        bool can = true;
        foreach (RoomUpgrade req in requirements)
        {
            if (!room.upgradeUnlocked.Contains(req))
            {
                can = false;
                break;
            }
        }
        return can;
    }

}
