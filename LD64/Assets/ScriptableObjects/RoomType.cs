﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;


public class RoomType : MonoBehaviour
{
    [Serializable]
    public class GenerationRates
    {
        public int nbCrew;
        public float rate;
    }

    public string name;
    public string key;
    public string description;
    public Sprite sprite;
    public List<GenerationRates> rates;
    public bool isProbRoom;

    [Serializable]
    public class RoomUpdateEvent : UnityEvent<float> { }

    public RoomUpdateEvent onRoomUpdate;

    public RoomUpgrade rank1_1, rank1_2, rank2_1, rank2_2, rank3;


    private float rateBoost;
    public HashSet<RoomUpgrade> upgradeUnlocked;


    private float currentRate;
    void Awake()
    {
        rateBoost = 0;
        upgradeUnlocked = new HashSet<RoomUpgrade>();
    }

    public float getRate(int nbCrew)
    {
        if (rates.Count < 1)
            return 0;

        int index = 0;
        while(index + 1 < rates.Count&& rates[index + 1].nbCrew <= nbCrew)
        {
            index++;
        }

        currentRate = rates[index].rate + rateBoost;
        return currentRate;
    }

    public void updateRoomEffect(Room room)
    {
        if(onRoomUpdate != null)
            onRoomUpdate.Invoke(getRate(room.getActiveWorkerCount()));
    }

    public void addUpgrade(RoomUpgrade upgrade)
    {
        //Debug.Log("ADDING UPGRADE : " + upgrade.key);
        //Debug.Log(name);
        upgradeUnlocked.Add(upgrade);
        switch (upgrade.key)
        {
            case "rateBoost":
                {
                    rateBoost += upgrade.value;
                    break;
                }
            case "lowerProbFoodCost":
                {
                    GameController.game.probeFoodCostBonus -= upgrade.value;
                    break;
                }
            case "lowerProbHumanCost":
                {
                    GameController.game.probeHumanCostBonus -= (int) upgrade.value;
                    break;
                }
            case "foodBoostCondition":
                {
                    GameController.game.foodBoostCondition = upgrade.conditionValue;
                    GameController.game.foodBoost = upgrade.value;
                    break;
                }
            case "killEnemyCrystalBonus":
                {
                    GameController.game.killEnemyCrystalBonus += upgrade.value;
                    break;
                }
            case "crystalBoostCondition":
                {
                    GameController.game.crystalBoostCondition = upgrade.conditionValue;
                    GameController.game.crystalBoost = upgrade.value;
                    break;
                }
            case "cloneExtraChance":
                {
                    GameController.game.cloneExtraChance = upgrade.value;
                    break;
                }
            case "cloneShieldBonus":
                {
                    GameController.game.cloneShieldBonus = upgrade.value;
                    break;
                }
            case "shieldProbeBonus":
                {
                    GameController.game.shieldProbeBonus = upgrade.value;
                    break;
                }
            case "enemyCountExtraDamageBoost":
                {
                    GameController.game.enemyCountExtraDamage = upgrade.conditionValue;
                    GameController.game.enemyCountExtraDamageBoost = upgrade.value;
                    break;
                }
            case "enemyDeathDamage":
                {
                    GameController.game.doubleKillChance = upgrade.value;
                    break;
                }
            case "shieldOverdrive":
                {
                    GameController.game.increaseTotaltShield(upgrade.value);

                    float shield = GameController.game.getShieldPoints() + upgrade.value;
        
                    GameController.game.setShieldPoints(shield);
                    
                    break;
                }
        }
    }

    public string getDescription()
    {
        string desc = description;
        switch (key)
        {
            case "food":
            {
                desc += "\n";
                desc += "Food production: " + currentRate + " per second\n";
                    break;
            }
            case "crystal":
                {
                    desc += "\n";
                    desc += "Crystal production: " + currentRate + " per second\n";
                    break;
                }
            case "clone":
                {
                    desc += "\n";
                    desc += "Cloning rate: 1 every " + currentRate + " second\n";
                    break;
                }
            case "shield":
                {
                    desc += "\n";
                    desc += "Shield regen: " + currentRate + " per second\n";
                    break;
                }
            case "weapon":
                {
                    desc += "\n";
                    desc += "Damage: " + currentRate + " per second\n";
                    break;
                }
        }
        return desc;
    }
}
