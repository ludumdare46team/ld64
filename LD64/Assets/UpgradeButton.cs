﻿using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeButton : MonoBehaviour
{
    private RoomUpgrade roomUpgrade;
    
    public void setRoomUpgrade(RoomUpgrade rU)
    {
        roomUpgrade = rU;
        GetComponent<Image>().sprite = roomUpgrade.icon;
    }
}
