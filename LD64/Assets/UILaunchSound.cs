﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UILaunchSound : MonoBehaviour
{
    public string soundId;

    public void LaunchSound()
    {
        GameSetup.setup.playEffect(soundId);
    }
}
