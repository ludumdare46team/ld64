﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
	void Update()
	{
		/*if (Input.GetKeyDown(KeyCode.Escape))
		{
			Application.Quit();
		}*/
	}

	public void quit()
	{
		Application.Quit();
	}

	public void start()
	{
		Initiate.Fade("Tuto", Color.black, 1f);
	}

	public void back()
	{
		Initiate.Fade("Menu", Color.black, 1f);
	}

	public void credit()
	{
		Initiate.Fade("Credits", Color.black, 1f);
	}

	public void setSound(float value)
	{
		GameSetup.setup.effectsVolume = value;
		GameSetup.setup.musicVolume = value;
		GameSetup.setup.currentMusic.volume = value;
	}
}
